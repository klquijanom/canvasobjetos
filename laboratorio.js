
function tiro_parabolico(){
    let lienzo = document.getElementById('miLienzo');
    let ctx = lienzo. getContext ('2d');
    let vo = document.getElementById("Vo");
    let wo = document.getElementById("Wo");
    /* intento 1 con ecuación cuadratica ubicando el centro de la parabola para luego poner círculos sobre esta
    let g=9.8;  //gravedad
    let radianes = (3.1416/180)*wo.value; //conversión grados a radianes
    let alcance_max= ((vo.value**2)* Math.sin( (radianes*2))/(g)); //cálculo mayor longitud en x
    let altura_max= ((vo.value**2)* (Math.sin( (radianes)**2))/g*2); //cálculo mayor longitud en y
    console.log (alcance_max); 
    console.log (altura_max);   //se imprime en consola para verificar los resultados de las operaciones

    ctx.beginPath(); 
    ctx.moveTo(0,500);
    ctx.quadraticCurveTo(alcance_max/2, altura_max, 500, 0); // se crea la parabola con (alcance_max/2,altura_max) como punto de control
    ctx.lineWidth = 5;
    ctx.stroke();     la ecuación cuadrática que se genera no corresponde al movimiento real
    */  

    //intento 2 con cláculo de coordenadas, ubicando con estas directamente los círculos
    let radianes = (3.1416/180)*wo.value;  //converión grados a radianes
    let tiempo=0.5;    // se toma un primer tiempo=0
    let g=9.8;       // gravedad
    let x = vo.value*(Math.cos(radianes))*tiempo; 
    let y = (vo.value*(Math.sin(radianes))*tiempo)- (0.5*g*(tiempo**2)); //cálculo de la primera posición para hacer el rectángulo
    console.log(vo.value);
    console.log(wo.value);
    console.log(radianes);
    console.log(x);
    console.log(y);
    ctx.beginPath();
    ctx.moveTo(0,500);
    ctx.lineTo(x,500-y);
    ctx.lineTo(x+7,(500-(y-1)));
    ctx.lineTo(12,500);
      ctx.closePath();
    ctx.stroke();    //se hace el cañón usando lineas y la primera posición del movimiento
    tiempo=1;    // ya se uso una primera posición para crear el cañón así que calcularan las demás luego de esta
    for(let i=1;i<500;i++){           // ciclo para crear los círculos y aumentar el tiempo
        let x= vo.value*(Math.cos(radianes))*tiempo;    //posición en x
        let y= (vo.value*(Math.sin(radianes))*tiempo)- (0.5*g*(tiempo**2));   //posición en y
        ctx.moveTo(x,500-y);
        ctx.arc(x, 500-y ,5 , 0, Math.PI*2, false);            
        ctx.stroke();
        ctx.fill();
        tiempo=tiempo+0.5;
        
     }

    }

   
